 /*******************************************************************************
*                   : TUSUR
* File              : main.c
* Compiler          : Armcc.exe V5.06
* Version           : 5.27
* Last modified     : 30.05.2019
* 
* Author            : Gayfullina Anastasiya
* Support mail      : lastig01.11@gmail.ru
* 
*
* Target MCU        : MCU: MDR1986
* Description       : Stack
* Individual number	: 3
*
********************************************************************************/


#include <stdlib.h>
#include <stdio.h>


#define var2

#ifdef var2
int function1(int a, int b, int c, int d, int e, int f) {
	return (a+b+c+d+e+f);
}

int main(void){
  int sum = function1(7,2,1,3,9,0);
  return 0;
}
#endif

#ifdef var1
int function1() {
	function1();
}

int main(void){
  function1();
  return 0;
}
#endif