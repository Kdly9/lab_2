 /*******************************************************************************
*                   : TUSUR
* File              : main.c
* Compiler          : Armcc.exe V5.06
* Version           : 5.27
* Last modified     : 21.05.2019
* 
* Author            : Gayfullina Anastasiya
* Support mail      : lastig01.11@gmail.ru
* 
*
* Target MCU        : MCU: MDR1986
* Description       : Work with bits. Bit-Band. 
* 
* Individual number	: 3
*
********************************************************************************/

#define REG0_ADDR 0x20000000
#define REG0 *((volatile insigned long *) (REG0_ADDR))

void set_bit(int bit_num) {
  *((volatile unsigned long *)(REG0_ADDR + 0x2000000 + bit_num*4)) = 1;
}

void reset_bit(int bit_num) {
  *((volatile unsigned long *)(REG0_ADDR + 0x2000000 + bit_num*4)) = 0;
}


struct byte {
    unsigned a0: 1;
    unsigned a1: 1;
    unsigned a2: 1;
    unsigned a3: 1;
    unsigned a4: 1;
    unsigned a5: 1;
    unsigned a6: 1;
    unsigned a7: 1;
};
 
union Byte {
    unsigned char value;
    struct byte bitfield;
};
 
static char byte = 0;

int main(void){
  
	  union Byte x;
    x.value = 3; //Bit Field
	
  
  
  
  //Bit Banding
  set_bit(0);
  set_bit(1);
  reset_bit(0);
  reset_bit(1);
  
  // Handwork with bits
  byte |= (1<<0); 
	byte |= (1<<1); // set 3
  byte &=~(1<<0); 
	byte &=~(1<<1); // reset to 0
  return 0;
}
